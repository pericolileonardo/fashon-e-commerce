import React from "react";
import { createRoot } from 'react-dom/client';
import { Provider } from "react-redux";
import { positions, transitions, Provider as AlertProvider } from "react-alert";
import AlertTemplate from "react-alert-template-basic";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import store from "./store";
import App from "./App"; // Assicurati di avere un solo import di App
import { BrowserRouter } from "react-router-dom"; 
import  WhatsAppWidget  from './component/components/chat/chat.component'; // 

import { ChatProvider } from './component/contexts/chat.context';





const theme = createTheme();
const options = {
  timeout: 5000,
  position: positions.BOTTOM_CENTER,
  transition: transitions.SCALE,
};

const root = createRoot(document.getElementById('root'));
root.render(
  <>
    {/* Wrap the entire application with BrowserRouter */}
    <BrowserRouter>
    
      <ThemeProvider theme={theme}>
        <Provider store={store}>
        
          <AlertProvider template={AlertTemplate} {...options}>
            <App />
            
          <ChatProvider>
            <WhatsAppWidget/>
            </ChatProvider>
           
          </AlertProvider>
        </Provider>
      </ThemeProvider>
    </BrowserRouter>
  </>,
);


