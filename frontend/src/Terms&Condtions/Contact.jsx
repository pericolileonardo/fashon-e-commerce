import React from "react";
import {
  Divider,
  Typography,
  Box,
  Button,
  TextField,
  FormControl,
  Select,
  MenuItem,
} from "@mui/material";
import { useAlert } from "react-alert";
import { useHistory } from "react-router-dom";
import { makeStyles } from "@mui/styles";
import MetaData from "../component/layouts/MataData/MataData";
const useStyles = makeStyles((theme) => ({
  root_contactus: {
    padding: "8rem 0",
    backgroundColor: "white",
    width: "100%",
    overflow: "hidden",
  },
  contact_Container_contactus: {
    width: "70%",
    margin: "0 auto",
  },
  title_contact_us: {
    color: "#414141",
    fontSize: "1.5rem !important",
    padding: "1rem 3rem",
    fontFamily: "Roboto",
    fontWeight: "700 !important",
    letterSpacing: "2px",
    [theme.breakpoints.down("sm")]: {
      fontSize: "14px ",
      padding: "1rem 0",
    },
  },
  divider_contact: {
    width: "90%",
    backgroundColor: "#b6b6b6",
    margin: "2rem 0 !important",
  },
  helpTitle_contact_us: {
    fontSize: "18px",
    color: "black",
    padding: "2rem 0",
  },
  para_contact: {
    paddingBottom: "3rem",
    marginLeft: "0.5rem",
    color: "#414141",
    lineHeight: "1.5rem",
    fontSize: "16px !important",
    width: "90%",
    letterSpacing: "2px",
    [theme.breakpoints.down("sm")] :{
      width : "100%"
    }
  },
  address_contacts: {
    paddingBottom: "3rem",
    marginLeft: "0.5rem",
    color: "#414141",
    lineHeight: "1.5rem",
    fontSize: "16px !important",
    width: "90%",
    letterSpacing: "2px",
  },
  buttonGroup: {
    "& > *": {
      margin: theme.spacing(2),
    },
    [theme.breakpoints.down("sm")]: {
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      gap: "1rem",
    },
  },
  supportButton: {
    backgroundColor: "#292929 !important",
    color: "white !important",
    width: "fit-content !important",
    padding: "0.8rem 2rem   !important",
    marginLeft: "3.3rem !important",
    borderRadius: "5px !important",
    "&:hover": {
      backgroundColor: "#ed1c24 !important",
      color: "white !important",
    },
    [theme.breakpoints.down("sm")]: {
      marginLeft: "15px !important",
    },
  },
  callButton: {
    backgroundColor: "#292929 !important",
    color: "white   !important",
    width: "fit-content     !important",
    padding: "0.8rem 2rem   !important",
    marginLeft: "1.3rem !important",
    borderRadius: "5px !important",
    "&:hover": {
      backgroundColor: "#ed1c24 !important",
      color: "white !important",
    },
    [theme.breakpoints.down("sm")]: {
      padding: "0.8rem 3.4rem   !important",
    },
  },
  formContainer_container: {
    marginTop: "1rem",
    display: "flex",
    flexDirection: "column",
  },
  formField_contact: {
    // marginBottom: "2rem",
    width: "100%",
  },
  submitButtons: {
    alignSelf: "flex-start",
    backgroundColor: "#292929 !important",
    color: "white   !important",
    width: "fit-content     !important",
    padding: "1rem 3rem   !important",
    borderRadius: "5px !important",
    "&:hover": {
      backgroundColor: "#ed1c24 !important",
      color: "white !important",
    },
  },
  SelectOption_contact: {
    width: "100%",
    marginBottom: "2rem",
    "& .MuiOutlinedInput-root_contactus": {
      "& fieldset": {
        borderColor: "#000",
        borderRadius: "none !important",
      },
      "&:hover fieldset": {
        borderColor: "#000",
        "&.Mui-focused fieldset": {
          borderColor: "#000",
        },
      },
    },
    "& .MuiSelect-root_contactus": {
      backgroundColor: "white",
      color: "black",
    },
    "& .MuiSelect-icon": {
      color: "black",
    },
    "& .MuiList-root_contactus": {
      backgroundColor: "white",
      color: "black",
    },
  },
  lableText_contact: {
    color: "#000",
    fontSize: "1rem",
    fontWeight: "500",
    marginBottom: "1rem",
  },
  menu_contact: {
    "& .MuiList-root_contactus": {
      backgroundColor: "white",
      color: "black",
    },
  },
}));

const ContactForm = () => {
  const classes = useStyles();
  const alert = useAlert();
  const history = useHistory();
  const handleCall = () => {
    window.location.href = "tel:+8171280446";
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    alert.success("Your message has been sent successfully");
    history.push("/");
  };

  return (
    <Box className={classes.root_contactus}>
      <MetaData  title={"Contact Us"}/>
      <div className={classes.contact_Container_contactus}>
        <Typography variant="h2" className={classes.title_contact_us}>
        Contattaci
        </Typography>

        <Divider className={classes.divider_contact} />

        <Typography variant="h4" className={classes.helpTitle_contact_us}>
        Ho bisogno di aiuto?
        </Typography>

        <Typography variant="body2" className={classes.para_contact}>
        Abbiamo a disposizione la chat dal vivo, cerca l'icona della chat in basso a destra
          nell'angolo della mano di questa pagina. Se non è presente, chiamaci al{" "}
          <strong
            style={{
              textDecoration: "underline",
              cursor: "pointer",
            }}
            onClick={handleCall}
          >
            8171280546
          </strong>
          .
        </Typography>

        <Typography variant="body2" className={classes.para_contact}>
          <span className={classes.para2}>Dal lunedì al venerdì 7:00-6:00</span>
          <br />
          <span className={classes.para2}>Sabato dalle 9:00 alle 16:00 </span>
          <br />
          <span className={classes.para2}>Domenica chiusi</span>
        </Typography>

        <Typography variant="body2" className={classes.para_contact}>
        Ci trovi fuori da questi orari? Compila il nostro modulo di supporto qui sotto e
          ci metteremo in contatto a breve.
        </Typography>

        <Typography variant="body2" className={classes.address_contacts}>
          <span style={{ fontWeight: "500", paddingBottom: "0.5rem" }}>
            Shop Store, Pvt Ltd.
          </span>
          <br />
          15130 Sec 22
          <br />
          Roma, it 00133
          <br />
          Italia
        </Typography>

        <div className={classes.buttonGroup}>
          <a href="#issue-select" style={{ textDecoration: "none" }}>
            <Button variant="contained" className={classes.supportButton}>
            Modulo di supporto
            </Button>
          </a>

          <Button
            variant="contained"
            className={classes.callButton}
            onClick={handleCall}
          >
            Chiamaci
          </Button>
        </div>

         <Divider className={classes.divider_contact} />
        <div className={classes.supportForm}>
          <Typography
            variant="h4"
            className={classes.title_contact_us}
            style={{ paddingBottom: "1rem" }}
          >
            Modulo di supporto
          </Typography>

          <Typography variant="body2" className={classes.para_contact}>
          Hai bisogno di una risposta più rapida? Cerca la nostra icona della chat sul lato destro
            di questa pagina.
          </Typography>

          <form
            className={classes.formContainer_container}
            onSubmit={handleSubmit}
          >
            <div className={classes.SelectOption_contact}>
              <Typography variant="body2" className={classes.lableText_contact}>
              PROBLEMA *
              </Typography>
              <FormControl className={classes.formField_contact}>
                <Select
                  labelId="issue-label"
                  id="issue-select"
                  defaultValue="e-commerce"
                  MenuProps={{
                    classes: { paper: classes.menu_contact },
                  }}
                >
                  <MenuItem value="e-commerce">E-Commerce</MenuItem>
                  <MenuItem value="app">App</MenuItem>
                </Select>
              </FormControl>
            </div>

            <div className={classes.SelectOption_contact}>
              <Typography variant="body2" className={classes.lableText_contact}>
                DETAIL *
              </Typography>
              <FormControl className={classes.formField_contact}>
                <Select
                  labelId="detail-label"
                  id="detail-select"
                  defaultValue="others"
                  MenuProps={{
                    classes: { paper: classes.menu_contact },
                  }}
                >
                  <MenuItem value="availability">Disponibilità</MenuItem>
                  <MenuItem value="return/exchange">Reso/Cambio</MenuItem>
                  <MenuItem value="technical-support">
                    Supporto tecnico
                  </MenuItem>
                  <MenuItem value="invoicing">Fatturazione</MenuItem>
                  <MenuItem value="tracking-info">Tracking Info</MenuItem>
                  <MenuItem value="others">Altro</MenuItem>
                </Select>
              </FormControl>
            </div>

            <div className={classes.SelectOption_contact}>
              <Typography variant="body2" className={classes.lableText_contact}>
                Lingua *
              </Typography>
              <FormControl className={classes.formField_contact}>
                <Select
                  labelId="language-label"
                  id="language-select"
                  defaultValue="english"
                  MenuProps={{
                    classes: { paper: classes.menu_contact },
                  }}
                >
                  <MenuItem value="english">Inglese</MenuItem>
                  <MenuItem value="italy">Italiano</MenuItem>
                  <MenuItem value="japanese">Japanese</MenuItem>
                  <MenuItem value="chinese">Chinese</MenuItem>
                  <MenuItem value="german">German</MenuItem>
                </Select>
              </FormControl>
            </div>

            <div className={classes.SelectOption_contact}>
              <Typography variant="body2" className={classes.lableText_contact}>
                {" "}
                EMAIL *
              </Typography>
              <FormControl className={classes.formField_contact}>
                <TextField
                  placeholder="Enter Your Email *"
                  id="email-input"
                  type="email"
                />
              </FormControl>
            </div>

            <div className={classes.SelectOption_contact}>
              <Typography variant="body2" className={classes.lableText_contact}>
                {" "}
                MESSAGGIO *
              </Typography>
              <FormControl className={classes.formField_contact}>
                <TextField
                  id="message-textarea"
                  multiline
                  rows={6}
                  variant="outlined"
                  placeholder="Enter Your Message *"
                />
              </FormControl>
            </div>
            <Button
              type="submit"
              variant="contained"
              className={classes.submitButtons}
            >
              Invia
            </Button>
          </form>
        </div> 
      </div>
    </Box>
  );
};

export default ContactForm;
