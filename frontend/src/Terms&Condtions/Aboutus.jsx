import React from "react";
import { Typography, Container, Grid, Button } from "@mui/material";
import { makeStyles } from "@mui/styles";
import MetaData from "../component/layouts/MataData/MataData";
import TermsImage from "../Image/about/tc.jpg";
import { Link } from "react-router-dom";
const useStyles = makeStyles((theme) => ({
  about_us: {
    paddingTop: "8rem",
    paddingBottom: "4rem",
    backgroundColor: "white !important",
    width: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },

  container_12: {
    padding: "2rem",
    textAlign: "center",

    backgroundColor: "white !important",
    maxWidth: "100%",
  },
  image_about: {
    width: "100%",
    height: "auto",
    marginTop: "3rem",
    marginBottom: "2rem",
  },
  title_about: {
    color: "#414141",
    fontSize: "14px",
    padding: "2rem 1rem 2rem",
    fontFamily: "Roboto",
    fontWeight: "500 !important",
  },
  heading12_about: {
    fontSize: "1rem",
    padding: "2rem 1rem 2rem",
    fontWeight: "400 !important",
    color: "#414141",
    textAlign: "center",
  },
  introText_about: {
    maxWidth: "800px",

    lineHeight: "1.5",
    margin: "1.5rem 0",
    color: "#292929",
    fontSize: "1.2rem",
    fontWeight: "400 !important",
    textAlign: "justify",
    padding: "0.8rem 1rem",
  },
  infoText_about: {
    lineHeight: "1.5",
    margin: "2rem 0",
    color: "#292929",
    fontSize: "1rem",
    fontWeight: "400 !important",
    textAlign: "justify",
    padding: "0.8rem 1rem",
  },
  buttonContainer_about: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    padding: "1rem 0",
    width: "100%",
    marginTop: "1rem",
  },
  button1_about: {
    backgroundColor: "#000000 !important",
    color: "white !important",
    width: "fit-content !important",
    padding: "0.8rem 2rem   !important",
    marginLeft: "3.3rem !important",
    borderRadius: "5px !important",
    "&:hover": {
      backgroundColor: "#ed1c24 !important",
      color: "white !important",
    },
  },
  button2_about: {
    backgroundColor: "#292929 !important",
    color: "white   !important",
    width: "fit-content     !important",
    padding: "0.8rem 2rem   !important",
    marginLeft: "1.3rem !important",
    borderRadius: "5px !important",
    "&:hover": {
      backgroundColor: "#ed1c24 !important",
      color: "white !important",
    },
  },
}));

const About_UsPage = () => {
  const classes = useStyles();

  return (
    <>
      <div className={classes.about_us}>
        <MetaData title={"About Us"} />
        <Container className={classes.container_12}>
          <Grid container spacing={4} justifyContent="center">
            <Grid item xs={12} sm={6}>
              <img
                src={TermsImage}
                alt="Shop"
                className={classes.image_about}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography
                variant="h2"
                component="h1"
                className={classes.title_about}
              >
                
              </Typography>
              
             
            </Grid>
          </Grid>
        </Container>
        <Container className={classes.container_12}>
          <Typography
            variant="h3"
            component="h1"
            className={classes.heading12_about}
          >
            Chi siamo
          </Typography>
          <Typography variant="body1" className={classes.infoText_about}>
          Benvenuti in Blackout Boutique, il vostro punto di riferimento per la moda giovanile di qualità.

          Originariamente nato come negozio fisico, Blackout ha rapidamente guadagnato una solida reputazione per la sua dedizione alla qualità e all'autenticità. Riconoscendo l'importanza e la crescente domanda del mercato online, abbiamo deciso di estendere la nostra presenza anche sul web, offrendo ai nostri clienti un'esperienza di shopping senza soluzione di continuità tra il mondo fisico e quello digitale.

          Una delle nostre promesse più ferme è quella di mantenere i prezzi tra i più bassi del mercato. Questo non significa compromessi sulla qualità. Al contrario, siamo fieri di offrire prodotti di alta qualità a prezzi accessibili, rendendo la moda di tendenza accessibile a tutti. La nostra filosofia è semplice: tutti meritano di vestire bene senza svuotare le proprie tasche.

          La fiducia e la trasparenza sono al centro della nostra missione. Crediamo che ogni cliente debba sentirsi valorizzato, e per questo ci sforziamo di costruire e mantenere relazioni solide e durature con tutti coloro che scelgono Blackout. Il rapporto tra venditore e cliente è sacro per noi, e ci impegniamo a garantire che ogni interazione sia caratterizzata da integrità, onestà e rispetto reciproco.

          Sappiamo quanto sia importante per i nostri clienti avere un supporto affidabile, soprattutto nell'era digitale. Per questo motivo, siamo orgogliosi di offrire un servizio clienti attivo 24 ore su 24, 7 giorni su 7. Che tu abbia una domanda, un dubbio o semplicemente voglia condividere un feedback, il nostro team è sempre qui per te, pronto ad assisterti in ogni fase del tuo viaggio con Blackout.

          Unisciti a noi e sperimenta un nuovo modo di fare shopping, dove qualità, fiducia e convenienza si incontrano per creare un'esperienza indimenticabile. Benvenuto nella famiglia Blackout.
          </Typography>
          
        </Container>
        <Container className={classes.container_12}>
          <Typography
            variant="h3"
            component="h1"
            className={classes.heading12_about}
          >
            La Nostra Missione
          </Typography>
          <Typography variant="body1" className={classes.infoText_about}>
          Presso Blackout boutique, la nostra missione è chiara e incisiva: rivoluzionare il mondo della moda rendendolo accessibile a tutti, senza mai scendere a compromessi sulla qualità e sull'integrità.

          Crediamo fermamente che la moda sia un'espressione di sé, un diritto e non un lusso. Per questo ci impegniamo ad offrire i migliori prodotti al prezzo più conveniente, permettendo a chiunque, indipendentemente dal budget, di esprimersi con stile e autenticità.

          La fiducia è la pietra angolare della nostra missione. Desideriamo che ogni cliente si senta parte integrante della famiglia Blackout, e per realizzare questo, ci sforziamo di costruire relazioni basate sulla trasparenza, l'onestà e il rispetto reciproco. Ogni interazione, ogni feedback e ogni prodotto riflette la nostra dedizione a creare una comunità dove venditore e cliente camminano fianco a fianco nella passione per la moda.
          </Typography>
          

          <div className={classes.buttonContainer_about}>
            <Link
              to="/products"
              style={{ textDecoration: "none", color: "none" }}
            >
              <Button variant="contained" className={classes.button1_about}>
              I nostri prodotti
              </Button>
            </Link>
            <Link
              to="/contact"
              style={{ textDecoration: "none", color: "none" }}
            >
              <Button variant="contained" className={classes.button2_about}>
                Contattaci
              </Button>
            </Link>
          </div>
        </Container>
      </div>
    </>
  );
};

export default About_UsPage;
