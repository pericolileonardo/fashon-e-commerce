import React from "react";
import "./Privacy.css";
import MetaData from "../component/layouts/MataData/MataData";
const TermsAndConditions = () => {
  return (
    <div className="privacy-policy-container">
      <MetaData title="Terms And Conditions" />
      <div className="container___">
        <h1>Termini e Condizioni di Vendita</h1>
        <p>
        Grazie per l'acquisto con noi! Apprezziamo davvero la tua attività e
          il tuo interesse per i nostri prodotti. Vogliamo assicurarci che tu abbia un buon
          prova ad acquistare dal nostro negozio online, Shop.com.
        </p>
        <p>
        Come per ogni esperienza di acquisto, ci sono alcuni termini e
          condizioni applicabili. Effettuando un ordine e acquistando un prodotto
          dal nostro sito Web, accetti i termini seguenti insieme ad altri termini
          sul nostro sito web, come le politiche di restituzione e garanzia, la nostra privacy
          politica e termini di utilizzo. Assicurati di rivedere tutto completamente
          e con attenzione in modo da essere informato sui tuoi diritti e obblighi.
        </p>
        <h2>Accettazione dei presenti Termini</h2>
        <p>
        Tu ("Cliente") puoi effettuare ordini di Prodotti con Shop
          ("noi", "nostro") tramite il nostro sito web. Effettuando un ordine, acconsenti a
          i presenti Termini e Condizioni di Vendita (“Termini”) e riconosciamo che noi
          fornirà i Prodotti soggetti ai presenti Termini. A meno che espressamente
          concordato o stabilito per iscritto dal Negozio, qualsiasi termine o
          condizione in qualsiasi ordine o altra forma o corrispondenza presente in qualsiasi
          modo incompatibile con le presenti Condizioni sarà inapplicabile e di n
          forza ed effetto di sorta.
        </p>
        <h2>Ordini</h2>
        <p>
        Tutti gli ordini sono soggetti all'accettazione del Negozio. Questo significa
          Il Negozio può, per qualsiasi motivo, rifiutarsi di accettare o annullare o
          limitare qualsiasi ordine, o quantità dell'ordine, indipendentemente dal fatto che l'ordine sia stato o meno
          confermato. La ricezione di una conferma d'ordine non significa ns
          l'accettazione del tuo ordine, né una conferma della nostra offerta
          vendere. Stiamo semplicemente confermando che abbiamo ricevuto il tuo ordine. Se noi
          annullare un ordine dopo che ti è già stato addebitato, Acquista
          ti rimborserà l'importo addebitato.
        </p>
        <h2>Offerta di prodotti</h2>
        <p>
          ATutte le descrizioni dei prodotti sul nostro sito Web sono soggette a modifiche in qualsiasi momento
          tempo senza preavviso, a nostra esclusiva discrezione. Ci riserviamo il diritto di
          modificare o interrompere un prodotto in qualsiasi momento. Abbiamo fatto ogni sforzo
          per visualizzare i colori e le immagini dei prodotti nel modo più accurato possibile
          possibile. Tuttavia, non possiamo garantire che la visualizzazione di
          qualsiasi colore sarà accurato e rifletterà fedelmente l'oggetto fisico
          al ricevimento.
        </p>
        <h2>Prezzo</h2>
        <p>
        Tutti i prezzi sono soggetti a modifiche fino al momento in cui effettui l'ordine
          ed è accettato dal Negozio. Prezzi dei prodotti visualizzati su
          questo sito web esclude le spese di spedizione, che sono calcolate e
          visualizzato in base all'opzione di spedizione per l'articolo selezionato
          quando concludi l'acquisto. Si prega di notare che i prezzi su questo
          il sito web potrebbe differire da quelli nei negozi che riforniscono il Negozio
          prodotti, poiché questi negozi sono indipendenti da Shop.
          Il negozio si riserva il diritto di modificare i prezzi per qualsiasi prezzo
          errori visualizzati dovuti a errore umano, malfunzionamento del computer o altro
          un'altra ragione. Dove viene identificato un errore di prezzo dopo averlo fatto
          inviato un ordine online, Shop ti avviserà dell'errore
          nei prezzi non appena ragionevolmente possibile e puoi scegliere di non farlo
          procedere con l'acquisto di qualsiasi prodotto(i) di cui è stato indicato il prezzo
          corretto.
        </p>
        <h2>Offerte Speciali</h2>
        <p>
        Di tanto in tanto, potremmo offrire promozioni speciali per alcuni o tutti
          i nostri prodotti, inclusi sconti, prodotti in edizione limitata o gratuiti
          spedizione. Queste offerte possono essere solo per un periodo limitato e
          Shop si riserva il diritto di modificare o interrompere tali offerte
          in ogni momento.
        </p>
        <h2>Tasse</h2>
        <p>
        I prezzi indicati per i prodotti venduti di seguito includono i prezzi applicabili
          le tasse. Il Cliente dovrà pagare e rimborsare il Negozio se paga
          tasse, ad eccezione di quelle basate sul reddito del Negozio.
        </p>
        <h2>Pagamenti</h2>
        <p>
        Tutti gli ordini devono essere pagati per intero prima della spedizione. Informazioni sul pagamento
          viene inviato dal cliente al momento dell'effettuazione di qualsiasi ordine, e
          l'evasione e la spedizione dell'ordine sono soggette a verifica
          informazioni sui pagamenti e disponibilità di fondi.
        </p>
        <h2>Spedizione</h2>
        <p>
        Le opzioni di spedizione disponibili verranno visualizzate durante il checkout
          processi. Qualsiasi periodo di tempo fornito da Shop per quando il
          il/i prodotto/i potrebbe essere spedito o consegnato è una stima in buona fede. Mentre noi
          fare del nostro meglio per rispettare tale lasso di tempo, non è una garanzia. Effettivo
          la consegna del tuo ordine può essere influenzata da molti eventi, alcuni dei quali
          sono fuori dal nostro controllo. Il negozio non può essere ritenuto responsabile per il ritardo
          consegne. Se non puoi più utilizzare un articolo a causa di un ritardo
          consegna, contattare immediatamente il nostro Servizio Clienti.
          Si prega di fare riferimento alla nostra politica di restituzione per le opzioni disponibili.
        </p>
        <p>
        Tutti i rischi di perdita o danneggiamento dei prodotti passeranno a te, oppure a
          persona da voi designata, al momento della presa di possesso fisico del
          (prodotti). Se gli articoli vengono persi o danneggiati durante il trasporto, contattare
          il nostro reparto di assistenza clienti.
        </p>
        <h2>Resi</h2>
        <p>
        Una volta che l'ordine è stato effettuato e accettato dal Negozio, il cliente
          non può annullare tale ordine accettato senza il preventivo consenso del Negozio
          consenso scritto. Il cliente può restituire i prodotti per un rimborso del
          prezzo di acquisto (escluse le spese di spedizione iniziali) più eventuali
          imposta applicabile. Il Cliente dovrà organizzare e pagare la spedizione di restituzione
          spese. I prodotti devono essere restituiti al Negozio entro
          trenta giorni dall'acquisto.
        </p>
        <h2>Garazia</h2>
        <p>
        Per ulteriori informazioni sulla nostra politica di garanzia, fare riferimento a
          la garanzia scritta allegata al prodotto.
        </p>
        <h2>Non rivendibile</h2>
        <p>
        I prodotti venduti sul nostro sito web sono destinati esclusivamente ai clienti finali e non
          per la rivendita. Ci riserviamo il diritto di rifiutare o annullare qualsiasi ordine se noi
          sospettare che tu stia acquistando prodotti per la rivendita.
        </p>
        <h2>Legge applicabile/Giurisdizione</h2>
        <p>
          These Terms shall be governed and construed in accordance with the
          laws of Singapore.
        </p>
        <h2>Dispute Resolution and Applicable Law</h2>
        <p>
        Il presente Contratto deve essere regolato e interpretato in conformità con
          tutte le leggi applicabili di volta in volta in vigore a Singapore. IL
          Le parti concordano che tutte le controversie, controversie o conflitti
          derivanti da o in relazione al presente Contratto, comprese le controversie su
          la sua esistenza, validità, conclusione, efficacia vincolante, violazione,
          modifica, scadenza e risoluzione, si farà riferimento a e
          finalmente risolta mediante arbitrato a Singapore amministrato dal
          Singapore International Arbitration Centre (“SIAC”) in conformità con
          il Regolamento Arbitrale della SIAC pro tempore vigente. IL
          la sede dell'arbitrato sarà Singapore. Il tribunale sarà composto
          di un (1) arbitro, nominato dal Presidente della Corte
          dell'Arbitrato della SIAC. La lingua dell'arbitrato sarà
          Inglese.
        </p>
        <h2>Indennizzo</h2>
        <p>
        Nella misura massima consentita dalla legge applicabile, l'utente accetta
          indennizzare e tenere indenne Shop da e contro qualsiasi cosa
          reclami, costi, procedimenti, richieste, perdite, costi di difesa (inclusi,
          senza limitazione, ragionevoli spese e spese legali) di qualsiasi tipo
          o natura derivante da o in connessione con l'utilizzo del nostro sito web
          o la tua violazione dei presenti Termini.
        </p>
        <h2>Intero accordo</h2>
        <p>
        I presenti Termini e Condizioni di Vendita costituiscono l'intero accordo
          tra le parti e sostituiscono tutti i precedenti e contemporanei
          accordi, proposte o dichiarazioni, scritte o orali, riguardanti
          il loro argomento.
        </p>
        <h2>Informazioni sui contatti</h2>
        <p>
        Se hai domande su questi Termini e Condizioni, per favore
          Contattaci al:
        </p>
        <p style={{ fontWeight: "500" }}>Shop</p>
        <p style={{ fontWeight: "500" }}>123 Main Street, City, Country</p>
        <p style={{ fontWeight: "500" }}>
          Email:{" "}
          <span style={{ fontWeight: "400" }}>info@Shop.com</span>
        </p>
      </div>
    </div>
  );
};

export default TermsAndConditions;
