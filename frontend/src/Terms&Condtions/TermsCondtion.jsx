import React from "react";
import "./TermsAndCondtion.css";
import MetaData from "../component/layouts/MataData/MataData";
import TermsImage from "../Image/about/tc.jpg";
const TermsAndConditionsPage = () => {
  return (
    <div className="terms-container">
      <MetaData title="Terms and Conditions" />
      <img
        src={TermsImage}
        alt="Terms and Conditions"
        className="terms-image"
      />
      <div className="terms-overlay">
        <h1 className="terms-title">TERMINI E CONDIZIONI</h1>
      </div>
      <div className="terms-content">
      <p>
  Grazie per aver fatto acquisti su Shop! Apprezziamo il tuo business e il tuo interesse nei nostri attrezzi da cricket. Vogliamo assicurarci che tu abbia un'esperienza positiva nell'acquisto dal nostro sito web.
</p>
<p>
  Effettuando un ordine e acquistando un prodotto dal nostro sito web, accetti i seguenti termini e condizioni, insieme alle nostre politiche di reso e garanzia, politica sulla privacy e termini d'uso. Ti preghiamo di esaminare tutto attentamente in modo da essere informato sui tuoi diritti e obblighi.
</p>
<h2>Accettazione di Questi Termini</h2>
<p>
  Tu ("Cliente") puoi effettuare ordini di Prodotti con Shop ("noi," "il nostro") tramite il nostro sito web o, in determinate circostanze, per telefono. Effettuando un ordine, acconsenti a questi Termini e Condizioni di Vendita ("Termini") e riconosci che forniremo i Prodotti soggetti a questi Termini. Qualsiasi termine o condizione in un ordine o in qualsiasi altra forma o corrispondenza che sia incompatibile con questi Termini sarà inapplicabile e privo di qualsiasi forza ed effetto, a meno che non sia espressamente concordato per iscritto da Shop.
</p>
<h2>Ordini</h2>
<p>
  Tutti gli ordini sono soggetti all'accettazione di Shop. Ci riserviamo il diritto di rifiutare, annullare o limitare qualsiasi ordine o quantità di ordine per qualsiasi motivo, anche dopo l'invio di una conferma d'ordine. Se annulliamo un ordine dopo che hai effettuato il pagamento, ti rimborseremo l'importo addebitato.
</p>
<h2>Offerta di Prodotti</h2>
<p>
  Tutte le descrizioni dei prodotti sul nostro sito web sono soggette a modifiche senza preavviso a nostra esclusiva discrezione. Ci riserviamo il diritto di modificare o interrompere un prodotto in qualsiasi momento. Pur facendo ogni sforzo per visualizzare i colori e le immagini dei prodotti con precisione, non possiamo garantire che il display del tuo dispositivo rifletta esattamente l'oggetto fisico.
</p>
<h2>Prezzo</h2>
<p>
  Tutti i prezzi sono soggetti a modifiche fino all'accettazione del tuo ordine da parte di Shop. I prezzi visualizzati sul sito web escludono le spese di spedizione, calcolate e visualizzate in base all'opzione di spedizione selezionata durante il checkout. I prezzi sul sito web possono differire da quelli dei negozi fisici che vendono prodotti Shop. Ci riserviamo il diritto di correggere errori di prezzo e di notificarti eventuali modifiche prima di procedere con il tuo ordine.
</p>
<h2>Offerte Speciali</h2>
<p>
  Di tanto in tanto, potremmo offrire promozioni speciali, tra cui sconti, prodotti in edizione limitata o spedizione gratuita. Queste offerte sono soggette a modifiche o interruzioni in qualsiasi momento.
</p>
<h2>Tasse</h2>
<p>
  I prezzi quotati per i Prodotti includono l'attuale Imposta sul Valore Aggiunto (IVA) di Singapore. Il Cliente è responsabile del pagamento di qualsiasi altra tassa, ad eccezione di quelle basate sul reddito di Shop. Se Shop è tenuta a riscuotere e pagare le tasse per conto del Cliente, potremmo fatturare al Cliente tali importi.
</p>
<h2>Pagamento</h2>
<p>
  Tutti gli ordini devono essere pagati integralmente prima della spedizione. Accettiamo pagamenti tramite MasterCard e Visa. Le informazioni di pagamento vengono fornite al momento dell'ordine e sono soggette a verifica e disponibilità di fondi.
</p>
<h2>Spedizione</h2>
<p>
  Le opzioni di spedizione disponibili verranno visualizzate durante il processo di checkout. Eventuali tempi di spedizione forniti sono stime e non possiamo garantire date di consegna esatte. Non siamo responsabili per consegne tardive, ma se non hai più bisogno di un articolo a causa di una consegna tardiva, ti preghiamo di contattare il nostro Servizio Clienti. Consulta la nostra politica di reso per le opzioni disponibili. Ogni rischio di perdita o danneggiamento dei prodotti passa a te al momento del ritiro fisico, e il titolo passa a te quando i prodotti vengono ritirati dal corriere di spedizione.
</p>
<h2>Resi</h2>
<p>
  Una volta effettuato e accettato un ordine, non puoi annullare l'ordine senza il consenso scritto di Shop. Puoi restituire i prodotti per un rimborso del prezzo d'acquisto (escluse le spese di spedizione iniziali) più qualsiasi tassa applicabile. Le spese di spedizione per il reso sono a carico del cliente. I prodotti devono essere restituiti entro trenta giorni dall'acquisto.
</p>
<h2>Garanzia</h2>
<p>
  Per informazioni sulla garanzia, consulta la garanzia scritta inclusa con il prodotto o la pagina della garanzia sul nostro sito web.
</p>
<h2>Non per la Rivendita</h2>
<p>
  I prodotti venduti sul nostro sito web sono destinati esclusivamente ai clienti finali e non per la rivendita. Ci riserviamo il diritto di rifiutare o annullare qualsiasi ordine se sospettiamo che i prodotti siano acquistati per la rivendita.
</p>
<h2>Legge Applicabile / Giurisdizione</h2>
<p>
  Questi Termini saranno regolati e interpretati in conformità alle leggi di Singapore.
</p>
<h2>Risoluzione delle Controversie e Legge Applicabile</h2>
<p>
  Eventuali controversie derivanti o relative a questi Termini saranno risolte mediante arbitrato a Singapore, amministrato dal Singapore International Arbitration Centre (SIAC), in conformità con le Norme di Arbitrato del SIAC. La lingua dell'arbitrato sarà l'inglese.
</p>
<h2>Indennizzo</h2>
<p>
  Accetti di indennizzare e tenere Shop indenne da qualsiasi reclamo, spese, azioni legali, perdite o richieste derivanti dalla violazione di questi Termini o dall'uso del tuo account da parte tua o di altri.
</p>
<h2>Intero Accordo</h2>
<p>
  Questi Termini costituiscono l'intero accordo tra Shop e il Cliente, e prevale su tutti gli accordi, rappresentazioni, garanzie e intese precedenti o contemporanee.
</p>
<h2>Separabilità</h2>
<p>
  Se una qualsiasi disposizione di questi Termini viene ritenuta invalida o inapplicabile, le disposizioni rimanenti rimarranno in pieno vigore ed effetto, e la disposizione invalida o inapplicabile sarà interpretata in modo da dare massimo effetto all'intento delle parti.
</p>
<h2>Esclusività</h2>
<p>
  I diritti, le responsabilità, gli obblighi e i rimedi relativi ai Prodotti sono esclusivamente quelli espressi in questi Termini. Le rinunce, le liberatorie, le limitazioni di responsabilità e i rimedi espressi in questi Termini si applicano anche in caso di inadempienza, negligenza, violazione del contratto, responsabilità oggettiva o altra causa d'azione.
</p>

      </div>
    </div>
  );
};

export default TermsAndConditionsPage;
