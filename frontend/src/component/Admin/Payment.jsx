import React, { useState, useEffect } from 'react';
import './Payment.css';
import Confetti from 'react-confetti';

// import Footer from '../layouts/Footer/Footer'; 
import Navbar from "../Admin/Navbar";
// Importa l'immagine
import logo from '../../Image/admin/logoAzienda.png';
const Logo = () => (
  <img src={logo} alt="Logo" />
);

const Container = ({ children }) => (
  <div className="containers">
    {children}
  </div>
);

const ProductDisplay = () => (
  
  
  <Container>
    <section className="product-section">
      <div className="product">
        <Logo />
        <div className="description">
  <h1>Piano Mensile E-commerce</h1>
  <h3>€20,00 / mese</h3>
  <p>Ad aderendo a questo piano mensile, avrai accesso completo al nostro sito web E-commerce. Potrai gestire tutti gli aspetti del tuo abbonamento dalla dashboard dedicata, rendendo facile tenere traccia delle tue spese, monitorare le tue transazioni e gestire le tue preferenze. Il nostro obiettivo è fornire un'esperienza utente fluida e senza interruzioni, assicurando che tu possa concentrarti sulla gestione del tuo business online.</p>
</div>

      </div>
      <form className="checkout-form" action="/api/v1/create-checkout-session" method="POST">
        <input type="hidden" name='2000' value='2000' />
        <button className="checkout-button" type="submit">
          Abbonati
        </button>
      </form>
    </section>
  </Container>
);

const SuccessDisplay = ({ sessionId }) => {
  
  const [showConfetti, setShowConfetti] = useState(true);

  useEffect(() => {
    const timer = setTimeout(() => {
      setShowConfetti(false);
    }, 5000);  // Nasconde l'effetto coriandoli dopo 5 secondi

    return () => clearTimeout(timer);  // Pulisce il timer se il componente viene smontato
  }, []);

  return (
    <div>
    <Navbar />
    <Container>
      {showConfetti && (
        <Confetti
          width={window.innerWidth}
          height={window.innerHeight}
        />
      )}
      <section className="success-section">
        <div className="product success-product">
          <Logo />
          <div className="description success-description">
            <h3>Sottoscrizione al piano iniziale riuscita!</h3>
          </div>
        </div>
        <form className="portal-form" action="/api/v1/create-portal-session" method="POST">
          <input
            type="hidden"
            id="session-id"
            name="session_id"
            value={sessionId}
          />
          <button className="portal-button" type="submit">
            Gestisci le tue informazioni di fatturazione
          </button>
        </form>
      </section>
    </Container>
    </div>
  );
};
const Message = ({ message }) => (
  <section>
    <p>{message}</p>
  </section>
);

export default function App() {
  let [message, setMessage] = useState('');
  let [success, setSuccess] = useState(false);
  let [sessionId, setSessionId] = useState('');

  useEffect(() => {
    // Check to see if this is a redirect back from Checkout
    const query = new URLSearchParams(window.location.search);

    if (query.get('success')) {
      setSuccess(true);
      setSessionId(query.get('session_id'));
    }

    if (query.get('canceled')) {
      setSuccess(false);
      setMessage(
        "Order canceled -- continue to shop around and checkout when you're ready."
      );
    }
  }, [sessionId]);

  return (
    <div>
      {(!success && message === '') ? <ProductDisplay /> : null}
      {success && sessionId !== '' ? <SuccessDisplay sessionId={sessionId} /> : null}
      {message ? <Message message={message} /> : null}
      {/* <Footer /> Inserisci il tuo footer qui */}
    </div>
  );
}



