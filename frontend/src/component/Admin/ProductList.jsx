import React, { useState, useEffect } from "react";
import "./ProductList.css";
import { DataGrid } from "@material-ui/data-grid";
import { useDispatch, useSelector } from "react-redux";
import {
  clearErrors,
  getAdminProducts,
  deleteProduct,
} from "../../actions/productAction";
import { Link, useHistory } from "react-router-dom";
import { useAlert } from "react-alert"; 

import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import MetaData from "../layouts/MataData/MataData";
import Loader from "../layouts/loader/Loader";
import Sidebar from "./Siderbar";
import Navbar from "./Navbar";
import { DELETE_PRODUCT_RESET } from "../../constants/productsConstatns";

function ProductList() {
  const dispatch = useDispatch();
  const alert = useAlert();
  const history = useHistory();
  const [toggle, setToggle] = useState(false);

  const { error, products, loading } = useSelector((state) => state.products);
  const { error: deleteError, isDeleted } = useSelector(
    (state) => state.deleteUpdateProduct
  );
  useEffect(() => {
    if (error) {
      alert.error(error);
      dispatch(clearErrors());
    }
   
    if (deleteError) {
      alert.error(deleteError);
      dispatch(clearErrors());
    }
    if (isDeleted) {
      alert.success("Product Deleted Successfully");
    
      dispatch({ type: DELETE_PRODUCT_RESET });
    }
    dispatch(getAdminProducts());
  }, [dispatch, error, alert, deleteError, history, isDeleted]);

  const deleteProductHandler = (id) => {
    dispatch(deleteProduct(id));
  };

const columns = [
  {
    field: "images",
    headerName: "Immagine",
    minWidth: 200,
    flex: 0.5,
    headerClassName: "column-header hide-on-mobile",
    renderCell: (params) => {
      const images = params.row.images;
      if (images && images.length > 0) {
        return (
          <img
            src={images[0].url} // Assuming the URL of the first image
            alt={params.row.name}
            style={{ width: 80, height: 80}}
          />
        );
      } else {
        return "Nessuna immagine";
      }
    },
  },
  
  
  {
    field: "name",
    headerName: "Nome",
    minWidth: 150,
    flex: 0.5,
    magin: "0 auto",
    headerClassName: "column-header hide-on-mobile",
  },
  
  
        
        {
          field: "sizeStock",
    headerName: "Magazzino",
    type: "string",
    minWidth: 280,
    flex: 0.5,
    headerClassName: "column-header hide-on-mobile",
    valueGetter: (params) => {
      const sizeStock = params.row.sizeStock;
      if (sizeStock) {
        return `XS: ${sizeStock.xs}, S: ${sizeStock.s}, M: ${sizeStock.m}, L: ${sizeStock.l}, XL: ${sizeStock.xl}, XXL: ${sizeStock.xxl}, U: ${sizeStock.U}`;
      } else {
        return "";
      }
    },
  },
  
  
  {
    field: "price",
    headerName: "Prezzo",
    type: "number",
    minWidth: 200,
    flex: 0.5,
    headerClassName: "column-header hide-on-mobile",
  },
  {
    field: "actions",
    headerName: "Modifica",
    flex: 1,
    sortable: false,
    minWidth: 200,
    headerClassName: "column-header1",
    renderCell: (params) => {
      return (
        <>
          <Link
            to={`/admin/product/${params.getValue(params.id, "id")}`}
            style={{ marginLeft: "1rem" }}
            >
            <EditIcon className="icon-" />
          </Link>

          <div
            onClick={() =>
              deleteProductHandler(params.getValue(params.id, "id"))
            }
          >
            <DeleteIcon className="iconbtn" />
          </div>
        </>
      );
    },
  },
  {
    field: "id",
    headerName: "ID Prodotto",
    minWidth: 230,
    flex: 0.5,
    headerClassName: "column-header",
  },

];


const rows = products.map((item) => ({
  
  id: item._id,
  images: item.images,
  
  
  xs: item.sizeStock.xs,
  s: item.sizeStock.s,
  m: item.sizeStock.m,
  l: item.sizeStock.l,
  xl: item.sizeStock.xl,
  xxl: item.sizeStock.xxl,
  U: item.sizeStock.U,
  
  
  price: item.price,
  name: item.name,
}));

  products &&
    products.forEach((item) => {
      rows.push({
        id: item._id,
        sizeStock: item.sizeStock,
        price: item.price,
        name: item.name,
        images: item.images,
      });
    });

  // togle handler =>
  const toggleHandler = () => {

    setToggle(!toggle);
  };

  // to close the sidebar when the screen size is greater than 1000px
  useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth > 999 && toggle) {
        setToggle(false);
      

      }
    };
       
          
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [toggle]);

  return (
    <>
      {loading ? (
        <Loader />
      ) : (
        <>
          <MetaData title={`ALL PRODUCTS - Admin`} />

          <div className="product-list" style={{ marginTop: 0 }}>
            <div className={!toggle ? "listSidebar" : "toggleBox"}>
              <Sidebar />
            </div>

            <div className="list-table">
              <Navbar toggleHandler={toggleHandler} />
              <div className="productListContainer">
                <h4 id="productListHeading">TUTTI I PRODOTTI</h4>

                <DataGrid
                  rows={rows}
                  columns={columns}
                  pageSize={10}
                  disableSelectionOnClick
                  className="productListTable"
                  autoHeight
                />
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
}

export default ProductList;
