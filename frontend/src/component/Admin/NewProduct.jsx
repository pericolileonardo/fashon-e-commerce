import React, { useEffect, useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useAlert } from "react-alert";
import MetaData from "../layouts/MataData/MataData";
import Loader from "../layouts/loader/Loader";
import Sidebar from "./Siderbar";
import { createProduct, clearErrors } from "../../actions/productAction";
import { useHistory } from "react-router-dom";
import { NEW_PRODUCT_RESET } from "../../constants/productsConstatns";
import InputAdornment from "@material-ui/core/InputAdornment";
import Box from "@material-ui/core/Box";
import DescriptionIcon from "@material-ui/icons/Description";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import AttachMoneyIcon from "@material-ui/icons/AttachMoney";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import CollectionsIcon from "@mui/icons-material/Collections";
import ShoppingCartOutlinedIcon from "@material-ui/icons/ShoppingCartOutlined";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import InfoIcon from "@mui/icons-material/Info";

import Navbar from "./Navbar";

import useStyles from "../User/LoginFromStyle";
import {
  Avatar,
  TextField,
  Typography,
  FormControl,
  Button,
} from "@material-ui/core";

function NewProduct() {
  const dispatch = useDispatch();
  const history = useHistory();
  const alert = useAlert();

  const { loading, error, success } = useSelector(
    (state) => state.addNewProduct
  );
  const [name, setName] = useState("");
  const [price, setPrice] = useState(0);
  const [description, setDescription] = useState("");
  const [category, setCategory] = useState("");
  const [sizeStock, setSizeStock] = useState({
    xs: 0,
    s: 0,
    m: 0,
    l: 0,
    xl: 0,
    xxl: 0,
    U: 0,
  });
  const [info , setInfo] = useState("")
  const [images, setImages] = useState([]);
  const [imagesPreview, setImagesPreview] = useState([]);
  const [isCategory] = useState(false);
  const fileInputRef = useRef();
  const [toggle, setToggle] = useState(false);
// Stato per le categorie esistenti
const [categories, setCategories] = useState([]);



// Stato per la nuova categoria da aggiungere
const [newCategory, setNewCategory] = useState('');
  const classes = useStyles();
  // togle handler =>
  const toggleHandler = () => {
    console.log("toggle");
    setToggle(!toggle);
  };

  const handleSizeChange = (size, value) => {
    setSizeStock((prevSizeStock) => ({
      ...prevSizeStock,
      [size]: value,
    }));
  };

  // const handleCategoryChange = (e) => {
  //   setCategory(e.target.value);
  //   setIsCategory(true);
  // };
  const handleCategoryChange = (event) => {
    setCategory(event.target.value);
  };

  const handleNewCategoryChange = (event) => {
    setNewCategory(event.target.value);
  };
  useEffect(() => {
    const fetchCategories = async () => {
      try {
        const response = await fetch('/api/v1/categories'); // Assicurati che l'URL corrisponda all'endpoint del server
        const data = await response.json();
        setCategories(data.map(cat => cat.name)); // Assumendo che 'data' sia un array di oggetti categoria
      } catch (error) {
        alert.error('Impossibile caricare le categorie');
      }
    };
  
    fetchCategories();
  }, [alert]);

  const addCategory = async () => {
    if (newCategory && !categories.includes(newCategory)) {
      try {
        const response = await fetch('/api/v1/categories/add', { // Sostituisci con il tuo endpoint API
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ name: newCategory })
        });
  
        if (response.ok) {
          setCategories([...categories, newCategory]);
          setCategory(newCategory);
          setNewCategory('');
        } else {
          alert.error('Errore nel salvare la categoria');
        }
      } catch (error) {
        alert.error('Errore di rete');
      }
    }
  };

  const handleImageUpload = () => {
    fileInputRef.current.click();
  };

  useEffect(() => {
    if (error) {
      alert.error(error);
      dispatch(clearErrors());
    }

    if (success) {
      alert.success("Product Created Successfully");
      history.push("/admin/dashboard");
      dispatch({ type: NEW_PRODUCT_RESET });
    }
  }, [dispatch, alert, error, history, success]);

  const createProductSubmitHandler = (e) => {
    e.preventDefault();
    const myForm = new FormData();
    myForm.set("name", name);
    myForm.set("price", price);
    myForm.set("description", description);
    myForm.set("category", category);
    Object.keys(sizeStock).forEach((size) => {
      myForm.set(`sizeStock.${size}`, sizeStock[size]);
    });
    myForm.set("info", info);
    images.forEach((currImg) => {
      myForm.append("images", currImg);
    });
    console.log("dati size", sizeStock);
    dispatch(createProduct(myForm));
  };

  const handleRemoveImage = (imageIndex) => {
    // Rimuovi l'immagine dalla preview utilizzando l'indice
    setImagesPreview((oldPreviews) =>
      oldPreviews.filter((_, index) => index !== imageIndex)
    );
  
    // Rimuovi l'immagine dall'array delle immagini utilizzando l'indice
    setImages((oldImages) => oldImages.filter((_, index) => index !== imageIndex));
  };
  

  const createProductImagesChange = (e) => {
    const files = Array.from(e.target.files);
  
    files.forEach((file) => {
      const reader = new FileReader();
      reader.onload = () => {
        if (reader.readyState === 2) {
          // Aggiungi la nuova immagine all'array esistente senza cancellare le vecchie
          setImages((oldImages) => [...oldImages, reader.result]);
          setImagesPreview((oldPreviews) => [...oldPreviews, reader.result]);
        }
      };
      reader.readAsDataURL(file);
    });
  };

 


  return (
    <>
      {loading ? (
        <Loader />
      ) : (
        <>
          <MetaData title={"New Product"} />
          <div className={classes.updateProduct}>
            <div
              className={
                !toggle ? `${classes.firstBox1}` : `${classes.toggleBox1}`
              }
            >
              <Sidebar />
            </div>

            <div className={classes.secondBox1}>
              <div className={classes.navBar1}>
                <Navbar toggleHandler={toggleHandler} />
              </div>

              <div
                className={`${classes.formContainer} ${classes.formContainer2}`}
              >
                <form
                  className={`${classes.form} ${classes.form2}`}
                  encType="multipart/form-data"
                  onSubmit={createProductSubmitHandler}
                >
                  <Avatar className={classes.avatar}>
                    <AddCircleOutlineIcon />
                  </Avatar>
                  <Typography
                    variant="h5"
                    component="h1"
                    className={classes.heading}
                  >
                    Crea prodotto
                  </Typography>
                  <TextField
                    variant="outlined"
                    fullWidth
                    className={`${classes.nameInput} ${classes.textField}`}
                    label="Nome Prodotto"
                    required
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <ShoppingCartOutlinedIcon
                            style={{
                              fontSize: 20,
                              color: "#414141",
                            }}
                          />
                        </InputAdornment>
                      ),
                    }}
                  />

                  <TextField
                    variant="outlined"
                    label="Prezzo"
                    value={price}
                    required
                    fullWidth
                    className={`${classes.passwordInput} ${classes.textField}`}
                    onChange={(e) => setPrice(e.target.value)}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment
                          position="end"
                          style={{
                            fontSize: 20,
                            color: "#414141",
                          }}
                        >
                          <AttachMoneyIcon />
                        </InputAdornment>
                      ),
                    }}
                  />
                  

                  <TextField
                    variant="outlined"
                    label="Informazioni sul prodotto"
                    value={info}
                    required
                    className={`${classes.passwordInput} ${classes.textField}`}
                    onChange={(e) => setInfo(e.target.value)}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment
                          position="end"
                          style={{
                            fontSize: 20,
                            color: "#414141",
                          }}
                        >
                          <InfoIcon />
                        </InputAdornment>
                      ),
                    }}
                  />

                  <div className={classes.selectOption}>
                    {!isCategory && (
                      <Typography variant="body2" className={classes.labelText}>
                        Scegli la categoria
                      </Typography>
                    )}
                    <FormControl className={classes.formControl}>
                    <TextField className={classes.nameInput}
                    value={newCategory}
                    onChange={handleNewCategoryChange}
                    label="Nuova Categoria"
                    
                    fullWidth/>

              
                  <Button  onClick={addCategory} className={classes.loginButton}   color="primary">
                    Aggiungi Categoria
                  </Button>

                  
                      <Select
                      
                        variant="outlined"
                        fullWidth
                        value={category}
                        onChange={handleCategoryChange}
                        className={classes.select}
                        
                        inputProps={{
                          name: "category",
                          id: "category-select",
                        }}
                        MenuProps={{
                          classes: {
                            paper: classes.menu,
                          },
                          anchorOrigin: {
                            vertical: "bottom",
                            horizontal: "left",
                          },
                          transformOrigin: {
                            vertical: "top",
                            horizontal: "left",
                          },
                          getContentAnchorEl: null,
                        }}
                      >
                        {!category && (
                          
                          <textField value="" className={classes.termsAndConditionsText}>
                            
                            <em>Scegli la categoria</em>
                           
                          </textField >
                        )}
                        {categories.map((cate) => (
                          
                          <MenuItem  key={cate} value={cate}>
                            
                            {cate}
                            {/* <div key={cate._id}>
                            {cate.name}
                            <button onClick={() => deleteCategory(cate._id)}>Elimina</button>
                            </div> */}
                          </MenuItem>
                          
                        ))}
                        

                        
                      </Select>
                      
      
                    </FormControl>
                  </div>
                  <TextField
                    variant="outlined"
                    fullWidth
                    className={classes.descriptionInput}
                    label="Descrizione Prodotto"
                    multiline
                    rows={1}
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <DescriptionIcon
                            className={classes.descriptionIcon}
                          />
                        </InputAdornment>
                      ),
                    }}
                  />

                  <div className={classes.root}>
                    <div className={classes.imgIcon}>
                      <CollectionsIcon
                        fontSize="large"
                        style={{ fontSize: 40 }}
                      />
                    </div>

                    <input
                      type="file"
                      name="avatar"
                      className={classes.input}
                      accept="image/*"
                      onChange={createProductImagesChange}
                      multiple
                      style={{ display: "none" }}
                      ref={fileInputRef}
                    />
                    <label htmlFor="avatar-input">
                      <Button
                        variant="contained"
                        color="default"
                        className={classes.uploadAvatarButton}
                        startIcon={
                          <CloudUploadIcon
                            style={{
                              color: "#FFFFFF",
                            }}
                          />
                        }
                        onClick={handleImageUpload}
                      >
                        <p className={classes.uploadAvatarText}>
                          Carica Immagine
                        </p>
                      </Button>
                    </label>
                  </div>

                  <Box className={classes.imageArea}>
                    {imagesPreview &&
                      imagesPreview.map((image, index) => (
                        <div key={index} className={classes.imageContainer}>
                          <img
                            key={index}
                            src={image}
                            alt="Product Preview"
                            className={classes.image}
                          />
                          <Button
                            variant="outlined"
                            color="secondary"
                            onClick={() => handleRemoveImage(index)}
                          >
                            Rimuovi
                          </Button>
                        </div>
                      ))}
                  </Box>

                  <Typography
                      variant="h5"
                      component="h3"
                      className={`${classes.passwordInput} ${classes.heading}`}
                    >
                      Magazzino
                    </Typography>

                  {["xs", "s", "m", "l", "xl", "xxl", "U"].map((size) => (
                    <TextField
                      key={size}
                      variant="outlined"
                      label={`Quantità ${size}`}
                      className={`${classes.passwordInput} ${classes.textField}`}
                      type="number"
                      value={sizeStock[size]}
                      onChange={(e) => handleSizeChange(size, e.target.value)}
                      fullWidth
                      style={{ marginTop: '5 vh' }}
                    />
                  ))}

                  <Button
                    variant="contained"
                    className={classes.loginButton}
                    fullWidth
                    type="submit"
                    disabled={loading ? true : false}
                  >
                    Crea
                  </Button>
                </form>
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
}
export default NewProduct;