// Componente React: CategoriesList.jsx

import React, { useEffect, useState } from 'react';
import './Categories.css';
import Sidebar from "./Siderbar";
// import "./ProductList.css";
function CategoriesList() {
  const [categories, setCategories] = useState([]);
  const [editingId, setEditingId] = useState(null);
  const [newName, setNewName] = useState('');

  // Carica le categorie dal server
  useEffect(() => {
    const fetchCategories = async () => {
      const response = await fetch('/api/v1/categories');
      const data = await response.json();
      setCategories(data);
    };

    fetchCategories();
  }, []);

  const deleteCategory = async (id) => {
    const response = await fetch(`/api/v1/categories/${id}`, { method: 'DELETE' });
    
    if (response.ok) {
      setCategories(categories.filter(cat => cat._id !== id));
    } else {
      // Gestisci l'errore qui, ad esempio mostrando un messaggio all'utente
    }
  };

  const startEditing = (category) => {
    setEditingId(category._id);
    setNewName(category.name);
  };

  const handleEdit = async (e) => {
    e.preventDefault();
    const response = await fetch(`/api/v1/categories/${editingId}`, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ name: newName }),
    });

    if (response.ok) {
      const updatedCategories = categories.map(cat => 
        cat._id === editingId ? { ...cat, name: newName } : cat
      );
      setCategories(updatedCategories);
      setEditingId(null);
      setNewName('');
    } else {
      // Gestisci eventuali errori qui
    }
  };
  

  return (
    <>
    <div className="product-list">
      <div className="listSidebar">
        <Sidebar />
      </div>
      
    <>
    
      <div className="categories-container">
        <h2>Categorie</h2>
        {editingId && (
          <form onSubmit={handleEdit} className="edit-form">
            <input
              type="text"
              value={newName}
              onChange={(e) => setNewName(e.target.value)}
            />
            <button className="edit-button" type="submit">Salva Modifiche</button>
          </form>
        )}
        <table className="categories-table">
          <thead>
            <tr>
              <th>Nome Categoria</th>
              <th>Azioni</th>
            </tr>
          </thead>
          <tbody>
            {categories.map(category => (
              <tr key={category._id}>
                <td>{category.name}</td>
                <td>
                  {!editingId && (
                    <button onClick={() => startEditing(category)} className="edit-button">
                      Modifica
                    </button>
                  )}
                  {!editingId && (
                    <button onClick={() => deleteCategory(category._id)} className="delete-button">
                      Elimina
                      
                    </button>
                    
                  )}
                </td>
              </tr>
              
            ))}
          </tbody>
        </table>
      </div>
      </>
      </div>
    
  </>
);
}
export default CategoriesList;
