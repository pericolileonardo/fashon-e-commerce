import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import {
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  Typography,
  Button,
  Box,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  FormControl,
  Select,
  MenuItem,
} from "@material-ui/core";
import Rating from "@material-ui/lab/Rating";
import { FitScreen } from "@mui/icons-material";
import { Link } from "react-router-dom";
import {dispalyMoney  ,generateDiscountedPrice} from "../DisplayMoney/DisplayMoney"
import { addItemToCart } from "../../actions/cartAction";
import { useDispatch } from "react-redux";


const useStyles = makeStyles((theme) => ({
  root: {
    width: "280px",
    height: FitScreen,
    margin: theme.spacing(2),
    backgroundColor: "white",
    currsor: "pointer",
  },
  media: {
  
    height: 200,
    width: "90%",
    objectFit: "cover",
    margin : "1rem 1rem 0 1rem"
   },
  button: {
    backgroundColor: "black",
    color: "white",
    borderRadius: 4,
    fontWeight: "bold",
    width: "100%",
    height: 45,
    "&:hover": {
      backgroundColor: "#ed1c24",
      color: "black",
      fontWeight: "bold",
    },
  },
  oldPrice: {
    textDecoration: "line-through",
    fontWeight: "bold",
    color: "rgba(0, 0, 0, 0.6)",
    marginRight: theme.spacing(1),
  },
  finalPrice: {
    fontWeight: "bold",
    fontSize: "1.2rem",
  },
  description: {
    fontSize: "0.8rem",
    fontWeight: 500, 
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    display: "-webkit-box",
    overflow: "hidden",
    textOverflow: "ellipsis",
    WebkitLineClamp: 3,
    WebkitBoxOrient: "vertical",
  },
}));

const ProductCard = ({ product }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedSize, setSelectedSize] = useState(""); 
  const classes = useStyles();
    let discountPrice = generateDiscountedPrice(product.price);
    discountPrice = dispalyMoney(discountPrice);
  const oldPrice = dispalyMoney(product.price);

  const availableSizes = ["xs", "s", "m", "l", "xl", "xxl", "U"]; // Replace with the actual available sizes for the product


  const handleOpenModal = () => {
    setIsModalOpen(true);
  };

  const handleCloseModal = () => {
    setIsModalOpen(false);
  };

  const handleSizeChange = (event) => {
    setSelectedSize(event.target.value);
  };

  

  
  
  const truncated =
    product.description
      .split(" ")
      .slice(0, 5)
      .join(" ") + "...";
      const  nameTruncated = product.name.split(" ").slice(0, 3).join(" ") + "...";


      const addTocartHandler = (id, qty) => {
        // Add the product with the selected size to the cart
        if (selectedSize) {
          dispatch(addItemToCart(id, qty, selectedSize));
          handleCloseModal(); // Close the modal after adding to cart
        } else {
          // Show an error message or handle as needed (size not selected)
        }
      };

      

  return (
    <Card className={classes.root}>
      <Link
        className="productCard"
        to={`/product/${product._id}`}
        style={{ textDecoration: "none", color: "inherit" }}
      >
        <CardActionArea>
          <CardMedia className={classes.media} image={product.images[0].url} />
          <CardContent>
            <Typography
              gutterBottom
              color="black"
              fontWeight="bold"
              style={{ fontWeight: "700" }}
            >
              {nameTruncated}
            </Typography>
            <Box display="flex" alignItems="center">
              <Rating
                name="rating"
                value={product.ratings}
                precision={0.1}
                readOnly
                size="small"
                style={{ color: "#ed1c24", marginRight: 8, fontWeight: "400" }}
              />
              <Typography variant="body2" color="textSecondary">
                ({product.numOfReviews})
              </Typography>
            </Box>
            <Typography
              variant="body2"
              color="textSecondary"
              component="div"
              className={classes.description}
            >
              {truncated}
            </Typography>
            <Box display="flex" alignItems="center">
            {oldPrice !== discountPrice && (
              <Typography variant="body1" className={classes.oldPrice}>
                {oldPrice}
              </Typography>
              )}

              <Typography variant="body1" className={classes.finalPrice}>
                {discountPrice}
              </Typography>
            </Box>
          </CardContent>
        </CardActionArea>
      </Link>
      <Box display="flex" justifyContent="center" p={2}>
        <Button
          variant="contained"
          className={classes.button}
          // onClick={() => addTocartHandler(product._id, 1)}
          onClick={handleOpenModal} // Open the size selection modal
        >
          Aggiungi al carello
        </Button>
      </Box>
      {/* Size Selection Modal */}
      <Dialog open={isModalOpen} onClose={handleCloseModal}>
        <DialogTitle>Seleziona la taglia</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Scegli la taglia desiderata per il prodotto.
          </DialogContentText>
          <FormControl fullWidth>
          <Select value={selectedSize} onChange={handleSizeChange}>
              {availableSizes.map((size) => (
                <MenuItem
                  key={size}
                  value={size}
                  disabled={!product.sizeStock[size]}
                  style={
                    !product.sizeStock[size]
                      ? { color: "red" }
                      : undefined
                  }
                >
                  {size}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseModal} color="textPrimary">
            Annulla
          </Button>
          <Button
            onClick={() => addTocartHandler(product._id, 1)}
            color="textPrimary"
            disabled={!selectedSize || !product.sizeStock[selectedSize]}
          >
            Aggiungi al carrello
          </Button>
        </DialogActions>
      </Dialog>
    </Card>
  );
};

export default ProductCard;
