import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";


import {  Checkbox, FormControl, FormControlLabel } from "@material-ui/core";
import DoneIcon from "@mui/icons-material/Done";
import CloseIcon from "@mui/icons-material/Close";
import Grid from "@material-ui/core/Grid"; // Aggiunto Grid

import {
  generateDiscountedPrice,
  calculateDiscount,
  dispalyMoney,
} from "../DisplayMoney/DisplayMoney";
import LocalShippingOutlinedIcon from "@mui/icons-material/LocalShippingOutlined";
import Rating from "@material-ui/lab/Rating";
import "./ProductDetails.css";
import { useSelector, useDispatch } from "react-redux";
import { useRouteMatch } from "react-router-dom";
import useActive from "../hook/useActive";
import ReviewCard from "./ReviewCard";
import {
  clearErrors,
  getProductDetails,
} from "../../actions/productAction";
import { useAlert } from "react-alert";
import MetaData from "../layouts/MataData/MataData";
import { addItemToCart } from "../../actions/cartAction";
import ShopLoader from "../layouts/loader/Loader";
import Button from "@mui/material/Button";
import { PRODUCT_DETAILS_RESET } from "../../constants/productsConstatns";

const ProductDetails = () => {
  const match = useRouteMatch();
  const dispatch = useDispatch();
  const alert = useAlert();

  const [quantity] = useState(1);
  const history = useHistory();



  const [selectedSize, setSelectedSize] = useState(""); 

  const [previewImg, setPreviewImg] = useState("");
  const { handleActive, activeClass } = useActive(0);

  const { product, loading, error , success  } = useSelector(
    (state) => state.productDetails
  );


useEffect(() => {
  if (error) {
    alert.error(error);
    dispatch(clearErrors);
  }
  if (success) {
    setPreviewImg(product.images[0].url);

    handleActive(0);
    dispatch({ type: PRODUCT_DETAILS_RESET });
  }
  dispatch(getProductDetails(match.params.id));
  // eslint-disable-next-line react-hooks/exhaustive-deps
}, [
  dispatch,
  error,
  alert,
  success,
  match.params.id,

]);


  // handling Add-to-cart
  const handleAddItem = () => {
    if (selectedSize) {
      console.log("data cart", match.params.id, quantity, selectedSize);
      dispatch(addItemToCart(match.params.id, quantity, selectedSize));
      alert.success("Item Added To Cart");
      // Reindirizza l'utente alla pagina del carrello
      history.push("/cart"); // Sostituisci "/carrello" con l'URL effettivo del carrello
    } else {
      alert.error("Seleziona una taglia prima di aggiungere all'acquisto.");
    }
  };
  
  
  




  // handling Preview image
  const handlePreviewImg = (images, i) => {
   
    setPreviewImg(images[i].url);
    handleActive(i);
  };

  

  // calculating Prices
  const finalPrice = generateDiscountedPrice(product.price);
  const discountedPrice = product.price - finalPrice;
  const newPrice = dispalyMoney(finalPrice);
  const oldPrice = dispalyMoney(product.price);
  const savedPrice = dispalyMoney(discountedPrice);
  const savedDiscount = calculateDiscount(discountedPrice, product.price);

  return (
    <>
      {loading ? (
        <ShopLoader />
      ) : (
        <>
          <div className="prodcutDetialsContainer">
            <MetaData title={product.name} />
            <section
              id="product_details"
              style={{ height: "auto" }}
              className="section"
            >
              <div className="product_container">
                <div className="wrapper prod_details_wrapper">
                  {/*=== Product Details Left-content ===*/}
                  <div className="prod_details_left_col">
                    <div className="prod_details_tabs">
                      {product.images &&
                        product.images.map((img, i) => (
                          <div
                            key={i}
                            className={`tabs_item ${activeClass(i)}`}
                            onClick={() => handlePreviewImg(product.images, i)}
                          >
                            <img src={img.url} alt="product-img" />
                          </div>
                        ))}
                    </div>
                    <figure className="prod_details_img">
                      <img src={previewImg} alt="product-img" />
                    </figure>
                  </div>

                  {/*=== Product Details Right-content ===*/}
                  <div className="prod_details_right_col_001">
                    <h1 className="prod_details_title">{product.name}</h1>
                    <h4 className="prod_details_info">
                      {product.info && product.info}
                    </h4>

                    <div className="prod_details_ratings">
                      <Rating
                        value={product.ratings}
                        precision={0.5}
                        readOnly
                        style={{ color: "black", fontSize: 16 }}
                      />
                      <span>|</span>
                      <Link
                        to="#"
                        style={{ textDecoration: "none", color: "#414141" }}
                      >
                        {product.numOfReviews} Giudizi
                      </Link>
                    </div>

                    <div className="prod_details_price">
                      <div className="price_box">
                        <h2 className="price">
                          {newPrice} &nbsp;
                          
                          <small className="del_price">
                          {oldPrice !== newPrice && (
                            <del>{oldPrice}</del>
                          )}
                          </small>
                        </h2>
                        {oldPrice !== newPrice && (
                        <p className="saved_price">
                          Risparmi: {savedPrice} ({savedDiscount}%)
                        </p>
                          )}
                        <span className="tax_txt">
                          (Incluso tutte le tasse)
                        </span>
                      </div>

                      <div className="badge">
                        {product.sizeStock &&
                        Object.values(product.sizeStock).some(
                          (quantity) => quantity > 0
                        ) ? (
                          <span className="instock">
                            <DoneIcon /> In magazzino
                          </span>
                        ) : (
                          <span className="outofstock">
                            <CloseIcon />
                            Esaurito
                          </span>
                        )}
                      </div>
                    </div>

                    <div className="seprator2"></div>

                    <div className="productDescription">
                      <div className="productDiscriptiopn_text">
                        <h4>Descrizione :</h4>
                        <p>{product.description}</p>
                      </div>
                      <div className="prod_details_offers">
                        <h4>Offerte e sconti</h4>
                        <ul>
                          <li>Nessun costo di commissione </li>
                          <li>Paga più tardi e usufruisci del rimborso</li>
                        </ul>
                      </div>
                      <div className="deliveryText">
                        <LocalShippingOutlinedIcon />
                        Consegnamo! Dì solo quando e come.
                      </div>
                    </div>
                    <div className="seprator2"></div>

                    <Grid
                      container
                      className="prod_details_additem"
                      spacing={3}
                      direction="column"
                    
                    >
                      <Grid item sm={12} md={6} lg={3}>
                        <h5>Scegli la taglia:</h5>
                        <FormControl component="fieldset">
                          {product.sizeStock &&
                            Object.keys(product.sizeStock).map((size) => (
                              <div key={size} className="taglia-item">
                                <FormControlLabel
                                  control={
                                    <Checkbox
                                      item
                                      md={2}
                                      checked={selectedSize === size}
                                      onChange={() => {
                                        if (product.sizeStock[size] > 0) {
                                          setSelectedSize(size);
                                        }
                                      }}
                                      name={size}
                                      color={
                                        product.sizeStock[size] > 0
                                          ? "textPrimary"
                                          : "secondary"
                                      }
                                      disabled={product.sizeStock[size] === 0}
                                    />
                                  }
                                  label={
                                    product.sizeStock[size] === 0 ? (
                                      <span style={{ color: "red" }}>
                                        {size} (Esaurito)
                                      </span>
                                    ) : (
                                      size
                                    )
                                  }
                                />
                              </div>
                            ))}
                        </FormControl>
                      </Grid>
                    </Grid>

                    <div className="additem">
                      <Button
                        variant="contained"
                        className="prod_details_addtocart_btn"
                        onClick={handleAddItem}
                        disabled={product.Stock <= 0}
                      >
                        Aggiungi al carrello
                      </Button>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <div className="reviewCard">
              <ReviewCard product={product} />
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default ProductDetails;