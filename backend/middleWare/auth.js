const asyncWrapper = require("../middleWare/asyncWrapper");
const userModel = require("../model/userModel");
const jwt = require("jsonwebtoken");
const ErrorHandler = require("../utils/errorHandler");
const Session = require('../model/PaymentAdminModel');
require("dotenv").config({ path: "backend/.env" });
const FRONTEND_URL = process.env.FRONTEND_URL;



exports.checkSubscriptionStatus = async (req, res, next) => {
  try {
    const user_id = req.user.id;  // Assumendo che l'ID dell'utente sia disponibile nel req.user
    
    // Verifica se la sessione per l'utente esiste già nel database
    const existingSession = await Session.findOne({ user: user_id });
    
    if (!existingSession) {
      // Se non esiste una sessione, presumibilmente l'abbonamento non è mai stato acquistato
      
      
      return res.status(403).json({ error: "Abbonamento non acquistato" });


  }
  
    
    const currentDate = new Date();
    if (existingSession.endDate <= currentDate) {
      // L'abbonamento è scaduto
      
       return res.status(402).json({ error:"Abbonamento scaduto"});
      

    }
    
    // Se tutto va bene, procedi
     next();
  } catch (error) {
    console.error('Errore durante la verifica dello stato dell\'abbonamento:', error);
    next(new ErrorHandler('Errore del server', 500));
  }
};



exports.isAuthentictedUser = asyncWrapper(async (req , res , next) =>{
    const { token } = req.cookies; 
// if there is no token found
if(!token){
    return res.status(401).json({ error: "Token mancante" });
}

// now verify that token with seceret key . 
    const deCodeToken = jwt.verify(token, process.env.JWT_SECRET);

    // now get user id from deCodeToken because when we make token in userSchema so we added userID in payLoad section. with that id get user and store inside req object .

    const user = await userModel.findById(deCodeToken.id); 

    req.user = user; // now we have user in req.user
 
    next();

})


      // taking role as param and converting it into array using spread operator. for using array method
exports.authorizeRoles = (...roles) =>{
 return (req , res , next) =>{
     if (roles.includes(req.user.role) ===false){ 
        return next(
            new ErrorHandler(`Role: ${req.user.role} is not allowed to access this resouce `,
                403)
        )
     }
   
    next();
 }
}  

