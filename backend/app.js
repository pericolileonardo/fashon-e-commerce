const express = require("express");
const app = express();
const errorMiddleware = require("./middleWare/error");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const fileUpload = require("express-fileupload");
const path = require("path");
const cors = require("cors");




const user = require("./route/userRoute");
const order = require("./route/orderRoute");
const product = require("./route/productRoute");
const payment = require("./route/paymentRoute");

const paymentAdmin = require("./route/paymentAdminRoute");
const imageRoutes = require('./route/imageAvatarRandomRoute'); 
const Categories = require('./route/categoriesRoute'); 


// Assicurati che il percorso sia corretto





app.use(cookieParser());
app.use(cors());
app.use(fileUpload());
app.use(errorMiddleware);
app.use(express.json());
app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));


app.use("/api/v1", product);
app.use("/api/v1", user);
app.use("/api/v1", order);
app.use("/api/v1", payment);

app.use("/api/v1", paymentAdmin);
app.use("/api/v1", imageRoutes);
app.use("/api/v1", Categories);







const __dirname1 = path.resolve();

app.use(express.static(path.join(__dirname1, "/frontend/build")));

app.get("*", (req, res) =>
  res.sendFile(path.resolve(__dirname1, "frontend", "build", "index.html"))
);


module.exports = app;
