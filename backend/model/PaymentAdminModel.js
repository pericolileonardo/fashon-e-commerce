const mongoose = require('mongoose');

// Definisci uno schema per il modello
const sessionSchema = new mongoose.Schema({
  session_id: {
    type: String,
    required: true,
  },
 
  user: {
    type: mongoose.Schema.ObjectId,
    ref: "userModel",
    required: true,
  },
  
  startDate: {
    type: Date,
    required: true,
  },
  endDate: {
    type: Date,
    required: true,
  },
  



});

// Crea un modello utilizzando lo schema
const Session = mongoose.model('Session', sessionSchema);

module.exports = Session;
