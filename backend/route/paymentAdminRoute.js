const express = require("express");
const router = express.Router();
const cookieParser = require("cookie-parser");

require("dotenv").config({ path: "backend/.env" });
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
const FRONTEND_URL = process.env.FRONTEND_URL;

const Session = require('../model/PaymentAdminModel');
const userController = require('../middleWare/auth');
//  const { checkSubscriptionStatus } = require('../middleWare/auth');

router.use(express.static('public'));
router.use(express.urlencoded({ extended: true }));
router.use(express.json());
router.use(cookieParser());

//  router.get('/abbonamento2', checkSubscriptionStatus);

router.post('/create-checkout-session', async (req, res) => {
  try {
    userController.isAuthentictedUser(req, res, async (err) => {
      if (err) {
        console.error('Errore durante l\'autenticazione dell\'utente:', err);
        res.sendStatus(401);
        return;
      }

      const user_id = req.user.id;

      // Recupera tutte le sessioni per questo utente dal database
      const sessions = await Session.find({ user: user_id });

      let activeSession = null;

      // Cicla su tutte le sessioni trovate
      for (let session of sessions) {
        const currentDate = new Date();
        if (session.endDate > currentDate) {
          // Se l'abbonamento è ancora attivo, conserva questa sessione come sessione attiva
          activeSession = session;
        } else {
          // Se la sessione è scaduta, eliminala dal database
          await Session.deleteOne({ _id: session._id });
        }
      }

      // Se esiste una sessione attiva, reindirizza l'utente
      if (activeSession) {
        res.redirect(303, `${FRONTEND_URL}/create-portal-session/?success=true&session_id=${activeSession.session_id}`);
        return;
      }

      // Se non esiste una sessione attiva o se tutte le sessioni sono state eliminate, crea una nuova sessione di checkout con Stripe
      const stripeSession = await stripe.checkout.sessions.create({
        billing_address_collection: 'auto',
        line_items: [
          {
            price: 'price_1Nz3z6ASFrJ98yo7C3GwWs86',
            quantity: 1,
          },
        ],
        client_reference_id: user_id,
        mode: 'subscription',
        success_url: `${FRONTEND_URL}/create-portal-session/?success=true&session_id={CHECKOUT_SESSION_ID}`,
        cancel_url: `${FRONTEND_URL}?canceled=true`,
      });

      // Reindirizza l'utente all'URL di checkout di Stripe
      res.redirect(303, stripeSession.url);
    });
  } catch (error) {
    console.error('Errore durante la creazione della sessione di checkout:', error);
    res.sendStatus(500);
  }
});



router.post('/create-portal-session', async (req, res) => {
  try {
    userController.isAuthentictedUser(req, res, async (err) => {
      if (err) {
        console.error('Errore durante l\'autenticazione dell\'utente:', err);
        res.sendStatus(401);
        return;
      }
      // // Chiama checkSubscriptionStatus qui per la verifica dell abbonamento
      // userController.checkSubscriptionStatus(req, res, async (error) => {
      //   if (error) {
      //       // Gestisci l'errore come preferisci
      //       console.error('Errore durante la verifica dello stato dell\'abbonamentoo:', error);
      //       return;
      //   }

    
      const returnUrl = FRONTEND_URL;
      const { session_id } = req.body;

      const user_id = req.user.id;

      // Verifica se la sessione per l'utente esiste già nel database
      const existingSession = await Session.findOne({ user: user_id });
      if (existingSession) {
        // Se esiste, recupera la sessione di checkout esistente da Stripe
        const checkoutSession = await stripe.checkout.sessions.retrieve(existingSession.session_id);
        
        // Crea una nuova sessione del portale di Stripe
        const portalSession = await stripe.billingPortal.sessions.create({
          customer: checkoutSession.customer,
          return_url: returnUrl,
        });

        // Reindirizza l'utente direttamente all'URL del portale di Stripe
        res.redirect(303, portalSession.url);
        return;
      }

      // Se non esiste una sessione, procedi con la creazione della sessione in Stripe
      const checkoutSession = await stripe.checkout.sessions.retrieve(session_id);
      const portalSession = await stripe.billingPortal.sessions.create({
        customer: checkoutSession.customer,
        return_url: returnUrl,
      });

      const startDate = new Date();
      const endDate = new Date(startDate);
      endDate.setMonth(endDate.getMonth() + 1);
      const currentDate = new Date();
      const isActive = endDate > currentDate;

      // Salva la sessione nel database
      const sessionData = await Session.create({
        session_id: session_id,
        user: user_id,
        startDate: startDate,
        endDate: endDate,
        isActive: isActive ,
      });

      await sessionData.save();

      res.redirect(303, portalSession.url);
    });
      // });

  } catch (error) {
    console.error('Errore durante la creazione della sessione del portale:', error);
    res.sendStatus(500);
  }
});



module.exports = router;